#!/usr/bin/env python3
#Title       : Config Loader
#Description : Provides a child class to ConfigParser with additional validation
#Author      : QuidsUp
#Date        : 2021-12-27
#Version     : 0.1
#Usage       : N/A this module is loaded as part of other modules

#Standard Imports
from configparser import ConfigParser
import os
import sys

#Local Imports
import lib.errorlogger

#Create logger
logger = lib.errorlogger.logging.getLogger(__name__)

validvalues = {
    'api': {
        'api_key' : {'req' : True, 'vartype' : 'hex'},
        'url' : {'req' : True, 'vartype' : 'url'},
    },
    'display': {
        'cols' : {'req' : False, 'vartype' : 'int', 'min' : 0, 'max' : 2048},
        'rows' : {'req' : False, 'vartype' : 'int', 'min' : 0, 'max' : 2048},
        'scroll_speed' : {'req' : False, 'vartype' : 'float', 'min' : 0.1, 'max' : 2.0},
    },
}

class NoTrackConfig(ConfigParser):
    def __init__(self, file_name: str) -> None:
        """
        1. Carry out parent init
        2. Find config file
        3. Validate users config
        """
        super(NoTrackConfig, self).__init__()

        config_file = self.find_config(file_name)

        self.read(config_file)
        self.validate_config()


    def config_error(self) -> None:
        """
        Display Config Error then Exit
        """
        print()
        print('Please create an notracklcd.cfg file with the following values:')
        print('[api]')
        print('api_key = 123-abc')
        print('url = http://192.168.1.2')
        print()
        print('[display]')
        print('cols = 16')
        print('rows = 3')
        sys.exit(2)


    def check_key_exists(self, section, key) -> None:
        """
        Error if key is not present in section
        """
        if key not in self[section]:
            logger.error(f'Missing value for {key} under section [{section}] of the config file')
            sys.exit(3)


    def isfloat(self, num: float) -> bool:
        """
        Checks if num is a float
        Returns
            True - num is a float
            False - num is not a float
        """
        try:
            float(num)
            return True
        except ValueError:
            return False


    def validate_key(self, section: str, key: str, values: dict):
        """
        Validate Key
        1. Check data type matches
        2. Carry out validation specified in validvalues
        Error when value is outside of specified range
        """
        if values['vartype'] == 'int':
            if not self[section][key].isdigit():
                logger.error(f'Invalid data type for {key} under section [{section}].\nExpecting an integer value between {values["min"]} and {values["max"]}')
                sys.exit(3)

            if int(self[section][key]) < values['min'] or int(self[section][key]) > values['max']:
                logger.error(f'Invalid value specified for {key} under section [{section}].\nValue should be between {values["min"]} and {values["max"]}')
                sys.exit(3)

        elif values['vartype'] == 'float':
            if not self.isfloat(self[section][key]):
                logger.error(f'Invalid data type for {key} under section [{section}].\nExpecting a float value between {values["min"]} and {values["max"]}')
                sys.exit(3)

            if float(self[section][key]) < values['min'] or float(self[section][key]) > values['max']:
                logger.error(f'Invalid value specified for {key} under section [{section}].\nValue should be between {values["min"]} and {values["max"]}')
                sys.exit(3)


    def find_config(self, file_name: str) -> str:
        """
        Attempts to find {file_name} in /etc/notrack, ~/.notrack and local folder
        Returns:
            Location first found
            Blank string on failure
        """

        if os.path.isfile(f'/etc/notrack/{file_name}'):
            return f'/etc/notrack/{file_name}'

        if os.path.isfile(f'~/.notrack/{file_name}'):
            return f'~/.notrack/{file_name}'

        if os.path.isfile(f'./{file_name}'):
            return f'./{file_name}'

        logger.error(f'Missing config file {file_name}')
        self.config_error()


    def validate_config(self):
        """
        Expected values are specified in validvalues dict
        req - Required
        vartype - Variable Type

        """

        for section, keys in validvalues.items():
            if section not in self:
                logger.error(f'Missing [{section}] section in config file')
                self.config_error()

            for key, values in keys.items():
                if values['req'] == True:
                    self.check_key_exists(section, key)

                if key not in self[section]:              #Not required, Not present
                    continue

                self.validate_key(section, key, values)






