#!/usr/bin/env python3
#Title       : NoTrack API Class
#Description :
#Author      : QuidsUp
#Date        : 2021-27-12
#Version     : 0.1
#Usage       : N/A this module is loaded elsewhere

"""
Carries out the following requests against NoTrack API
count_blocklisted_domains: Get the number of domains in the block list
count_total_queries      : Get the total number of queries
get_status:              : Bitwise status of NoTrack (see status codes below)
recent_queries           : List of the recent DNS queries
"""

#Global Imports
import configparser
import math
import re
import signal
import sys
import time

#Local Imports
from lib.configloader import NoTrackConfig
from lib.apihandler import NoTrackAPI
import locales.localehandler
import lib.errorlogger

#3rd Party Imports
"""try:
    import pynput.keyboard as keyboard
except ModuleNotFoundError:
    print(f'ERROR {__name__} missing module: pynput')
    print('Please install with: pip3 install pynput')
    sys.exit(1)
"""

#Create logger
logger = lib.errorlogger.logging.getLogger(__name__)
_ = locales.localehandler.translate

Regex_Domain = re.compile('([\w\-]{1,63}(\.(?:co\.|com\.|org\.|edu\.|gov\.)?[\w\-]{1,63})$)')

class NoTrackDisplay:
    cols = 0
    rows = 0
    cursor = 0
    mode = 0
    scroll_speed = 0
    __notrack_status = 0

    masterdata = list()
    displaydata = list()
    events = list()

    __endloop = False


    def __init__(self) -> None:
        self.api = NoTrackAPI()

        signal.signal(signal.SIGINT, self.__sigkill)  #2 Inturrupt
        signal.signal(signal.SIGABRT, self.__sigkill) #6 Abort
        signal.signal(signal.SIGTERM, self.__sigkill) #9 Terminate


    def __exit_loop(self):
        logger.info('Closing NoTrack Display')
        self.__endloop = True


    def __sigkill(self, signum, frame):
        self.__exit_loop()


    def action_event(self, ev, mode):
        """
        """
        if ev['action'] == 'move_cursor':
            self.move_cursor(ev['x'], ev['y'])

        if ev['action'] == 'update':
            self.set_mode(mode)

        elif ev['action'] == 'exit':
            self.__exit_loop()


    def calculate_cursor(self, duration):
        """
        Calculate the cursor points to cycle through a large volume of data on a small screen
        Tries to match screen movement to scroll_speed by either skipping rows or showing the data multiple passes
        Outputs cursor position and time offset to self.events
        """
        currenttime = time.time()
        steps = 0.0
        passes = 0.0
        skip = 1
        timeoffset = 0

        cursorevent = dict()

        mastersize = len(self.masterdata)

        if self.rows >= mastersize:
            self.move_cursor(0, 0)
            return


        step_duration = duration / mastersize
        passes = step_duration / self.scroll_speed

        #Pass time below 50% of scroll_speed will require a row skip
        if passes < 0.5:
            passes = 1
            #Goal seek a comfey row skip
            for i in range(1, self.rows):
                step_duration = duration / (mastersize / i)
                #Check duration with skips will be within 50% of read time
                if (step_duration / self.scroll_speed) > 0.5:
                    skip = i
                    break

            #Failed to find a comfey skip, will just have to go as fast as possible
            if skip == 1:
                skip = self.rows
                step_duration = duration / (mastersize / self.rows)

        #50% to 100% of scroll_speed is close enough, round passes up to 1
        elif passes < 1:
            passes = 1
        else:
            passes = round(passes)
            step_duration = duration / (mastersize * passes)

        #print(f'Total Duration {duration}')
        #print(f'Step Duration {step_duration}')
        #print(f'List Size {mastersize}')
        #print(f'Read Time {self.scroll_speed}')
        #print(f'Passes {passes}')
        #print(f'Skip {skip}')

        for i in range(0, passes):
            for j in range(0, mastersize, skip):
                cursorevent = {
                    'action' : 'move_cursor',
                    'x' : 0,
                    'y' : j,
                    'time' : currenttime + (timeoffset * step_duration),
                }
                self.events.append(cursorevent)
                timeoffset += 1

        #self.events.pop(0)                                #Prevent duplicate showing


    def load_config(self) -> object:
        """
        Read users settings from notracklcd.cfg

        Returns:
            config class to allow child to get further configs
        """
        config = NoTrackConfig('notracklcd.cfg')
        language = 'en'

        self.cols = config.getint('display', 'cols', fallback=16)
        self.rows = config.getint('display', 'rows', fallback=3)
        self.mode = config.getint('display', 'mode', fallback=1)
        self.scroll_speed = self.rows ** config.getfloat('display', 'scroll_speed', fallback=1.4)
        language = config.get('display', 'language', fallback='en')

        #Setup locales in use based on display size
        if self.cols < 32:
            locales.localehandler.setuplocale(language, 'short')
        else:
            locales.localehandler.setuplocale(language, 'long')

        return config


    def move_cursor(self, x: int, y: int) -> None:
        """
        """
        self.cursor = y
        self.move_data()
        self.screen_clear()
        self.screen_display()

    def move_data(self) -> None:
        """
        """
        startpoint = 0
        endpoint = 0
        mastersize = 0

        mastersize = len(self.masterdata)
        startpoint = self.cursor
        endpoint = startpoint + self.rows
        if endpoint > mastersize:
            endpoint = mastersize

        self.displaydata = list()

        for i in range(startpoint, endpoint):
            self.displaydata.append(self.masterdata[i])


    def test_data(self) -> None:
        """
        """
        self.masterdata = [
            '1. All work and no play makes Jack a dull boy',
            '2. All work and no play makes Jack a dull boy',
            '3. All work and no play makes Jack a dull boy',
            '4. All work and no play makes Jack a dull boy',
            '5. All work and no play makes Jack a dull boy',
            '6. All work and no play makes Jack a dull boy',
            '7. All work and no play makes Jack a dull boy',
            '8. All work and no play makes Jack a dull boy',
            '9. All work and no play makes Jack a dull boy',
        ]

        self.calculate_cursor(300)
        self.events.append({'action' : 'exit', 'time' : time.time() + 30})


    def calculate_percent(self, first: int, second: int) -> str:
        if first == 0 or second == 0:
            return '0.00%'

        return f'{(first / second) * 100:.3g}%'

    def calculate_sigfig(self, num: float) -> str:
        """
        Returns a number to 3 significant figures, including scientific notation
        """
        if num < 1000:
            return f'{num:.3g}'

        elif num < 1000000:
            return f'{num / 1000:.3g}K'

        elif num < 1000000000:
            return f'{num / 1000000:.3g}M'

        return f'{num / 1000000000:.3g}B'


    def center_allign(self, text: str) -> str:
        return f'{text : ^{self.cols}}'

    def mode_blocklisted_domains(self) -> None:
        """
        Show the number of domains in the NoTrack blocklist
        """
        total = ''
        totalint = 0

        data = self.api.count_blocklisted_domains()       #Download from API

        totalint = int(data['total'])                     #Total is stored as str

        #Add comma seperator for less than 100K
        if totalint < 100000:
            total = f"{totalint:,}"
        #Use 3 significant figures for over 100K
        else:
            total = self.calculate_sigfig(totalint)

        self.masterdata = list()
        self.masterdata.append(self.center_allign(_('domains in blocklist')))
        self.masterdata.append(self.center_allign(f"{total} {_('domains')}"))

        self.calculate_cursor(120)
        self.events.append({'action' : 'update', 'time' : time.time() + 120})


    def mode_queries_time(self) -> None:
        """
        Recent Queries by Time
        Displays 3 different views depending on number of columns available
            1. Small  - less than 34 chars: Severity, Domain (no table)
            2. Medium - 35 to 64 chars    : Severity, Time, Domain (table layout)
            3. Large  - More than 65 chars: Severity, Time, Domain, SysIP (table layout)
        """
        colwidth_domain = 0
        colwidth_sysip = 0
        severity = ''
        query = ''
        qtime = ''
        sysip = ''
        rowseperator = ''
        borderbottom = ''

        #0 - #
        #1 - Date Time
        #2 - Sys IP
        #3 - Query
        #4 - Severity
        #5 - Action

        self.masterdata = list()

        #Setup column sizes depending on display width
        if self.cols < 34:                                #Small
                colwidth_domain = self.cols - 1

        elif self.cols < 64:                              #Medium
            colwidth_domain = self.cols - 12
            borderbottom = f'└─┴{"─"*8}┴{"─"*colwidth_domain}┘'
            rowseperator = f'├─┼{"─"*8}┼{"─"*colwidth_domain}┤'
            #Table Header
            self.masterdata.append(f'╒═╤{"═"*8}╤{"═"*colwidth_domain}╕')
            self.masterdata.append(f'│─│{"Time" :^8}│{"Query" :^{colwidth_domain}}│')
            self.masterdata.append(f'╞═╪{"═"*8}╪{"═"*colwidth_domain}╡')

        else:                                             #Large
            colwidth_sysip = math.floor((self.cols - 16) / 3.2)
            colwidth_domain = self.cols - 18 - colwidth_sysip #Extra 2 for col sep
            borderbottom = f'└───┴{"─"*10}┴{"─"*colwidth_domain}┴{"─"*colwidth_sysip}┘'
            rowseperator = f'├───┼{"─"*10}┼{"─"*colwidth_domain}┼{"─"*colwidth_sysip}┤'
            #Table Header
            self.masterdata.append(f'╒═══╤{"═"*10}╤{"═"*colwidth_domain}╤{"═"*colwidth_sysip}╕')
            self.masterdata.append(f'│ ─ │{"Time" :^10}│{"Query" :^{colwidth_domain}}│{"System" :^{colwidth_sysip}}│')
            self.masterdata.append(f'╞═══╪{"═"*10}╪{"═"*colwidth_domain}╪{"═"*colwidth_sysip}╡')


        data = self.api.queries_time(4)                   #Download from API

        for line in data['queries']:
            qtime = line[1][-8:]                          #Extract time from date/time
            sysip = line[2]
            query = self.simplify_domain(line[3], colwidth_domain)

            if line[4] == '1':
                severity = '+'                            #Allowed
            else:
                severity = '-'                            #Blocked

            #Output table to screen depending on display width
            if self.cols < 34:                            #Small
                self.masterdata.append(f'{severity}{query}')

            elif self.cols < 64:                          #Medium
                self.masterdata.append(f'│{severity}│{qtime}│{query : <{colwidth_domain}}│')
                self.masterdata.append(f'{rowseperator}')

            else:                                         #Large
                sysip = self.simplify_domain(sysip, colwidth_sysip)
                self.masterdata.append(f'│ {severity} │ {qtime} │{query : <{colwidth_domain}}│{sysip : ^{colwidth_sysip}}│')
                self.masterdata.append(f'{rowseperator}')


        #Replace line seperator with a proper bottom border for table views only
        if rowseperator != '':
            self.masterdata.pop()
            self.masterdata.append(borderbottom)

        self.calculate_cursor(240)
        self.events.append({'action' : 'update', 'time' : time.time() + 240})


    def mode_queries_today(self) -> None:
        """
        Show the number of blocked and allowed DNS queries for today
        """
        data = self.api.count_queries_today()             #Download from API

        self.masterdata = list()
        self.masterdata.append(self.center_allign(_('total queries')))
        self.masterdata.append(f"{_('queries allowed')}: {data['allowed']}")
        self.masterdata.append(f"{_('queries blocked')}: {data['blocked']}")

        self.calculate_cursor(120)
        self.events.append({'action' : 'update', 'time' : time.time() + 120})


    def mode_title(self, currentmode) -> None:
        """
        Title Screen for Mode Selected
        """
        if currentmode == 1:
            self.masterdata.append(self.center_allign(f"{_('mode')} 1"))
            self.masterdata.append(self.center_allign(_('total queries')))

        elif currentmode == 2:
            self.masterdata.append(self.center_allign(f"{_('mode')} 2"))
            self.masterdata.append(self.center_allign(f"{_('domains in blocklist')} {_('domains')}"))

        elif currentmode == 3:
            self.masterdata.append(self.center_allign(f"{_('mode')} 3"))
            self.masterdata.append(self.center_allign(f"Recent Queries"))
            self.masterdata.append(self.center_allign(f"By Time"))

        #Show title screen for 2 seconds and then refresh
        self.calculate_cursor(2)
        self.events.append({'action' : 'update', 'time' : time.time() + 2})


    def notrack_status_check(self) -> bool:
        """
        Gets the status value of NoTrack from the API
        Returns:
            True - Status Changed
            False - No Update
        """
        livestatus = 0

        livestatus = self.api.get_status()

        if livestatus == 0:                            #Invalid return
            return False

        if livestatus != self.__notrack_status:
            self.notrack_status_updated(livestatus)
            self.__notrack_status = livestatus
            return True

        return False


    def simplify_domain(self, domain: str, maxlen: int) -> str:
        """
        Shorten domain to fit into domain colum, based on maxlen
        Try to allow a 1/3-2/3 split between subdomain and topdomain
        Depending on space limitations, only the first few chars of subdomain will
        be displayed, followed by an *
        With more space available, a cutpoint will be made from left-hand side to
        right-hand side and joined by a ^
        """
        subdomain = ''
        topdomain = ''
        subdomainlen = 0
        topdomainlen = 0
        leftcut = 0
        rightcut = 0
        lowerthird = 0                                    #1/3-2/3 split
        upperthird = 0

        if len(domain) <= maxlen:                         #Length short enough
            return domain

        matches = Regex_Domain.search(domain)             #Extract topdomain using regex

        if matches is None:                               #No match shouldn't happen
            return domain                                 #Avoid errors to be sure

        topdomain = matches[1]
        topdomainlen = len(topdomain)

        if domain == topdomain:                           #Top domain only - no subdomain
            leftcut = math.floor(maxlen / 2)
            rightcut = maxlen - leftcut

            if domain[-rightcut] == '.':                  #Avoid cutting domain on a dot
                leftcut += 1                              #Shift cut to the right 1 char
                rightcut -= 1

            return f'{domain[0:leftcut]}^{domain[-rightcut:]}'


        #Dealing with subdomain and topdomain
        subdomain = domain[0:-topdomainlen]
        subdomainlen = len(subdomain)

        lowerthird = math.floor(maxlen / 3)
        upperthird = maxlen - 4

        #Does top domain leave less than 4 chars spare?
        #Shorten topdomain, then show first letter of subdomain, followed by *.
        if topdomainlen > upperthird:
            topdomain = self.simplify_domain(topdomain, upperthird)
            return f'{subdomain[0:1]}*.{topdomain}'

        #Now work on the 1/3-2/3 split
        upperthird = maxlen - lowerthird

        #Is topdomain longer than 2/3 and subdomain longer than 1/3+50%?
        #Shorten topdomain to 2/3
        #Show only the first 1/3 length of subdomain, followed by *
        if topdomainlen > upperthird and subdomainlen > (lowerthird * 1.5):
            topdomain = self.simplify_domain(topdomain, upperthird - 2)
            return f'{subdomain[0:lowerthird-1]}*.{topdomain}'

        #Is topdomain shorter than 2/3 and subdomain longer than 1/3+50%
        #Split subdomain in two to fit it into the remaining length
        elif topdomainlen <= upperthird and subdomainlen > (lowerthird * 1.5):
            lowerthird = (maxlen - topdomainlen) - 2
            leftcut = math.floor(lowerthird / 2)
            rightcut = lowerthird - leftcut
            return f'{subdomain[0:leftcut+1]}^{subdomain[-rightcut:]}{topdomain}'

        #Otherwise shorten subdomain to 1/3, followed by a *
        else:
            leftcut = lowerthird - 2
            topdomain = self.simplify_domain(topdomain, upperthird)
            return f'{subdomain[0:leftcut]}*.{topdomain}'

        return domain


    def set_mode(self, currentmode, showtitle=False) -> None:
        """
        """
        self.events = list()                              #Reset events

        if showtitle:
            self.mode_title(currentmode)
            return

        if currentmode == 1:
            self.mode_queries_today()

        elif currentmode == 2:
            self.mode_blocklisted_domains()

        elif currentmode == 3:
            self.mode_queries_time()


    def start_loop(self):
        """
        Main loop

        Utilises a queueing system for events which are timestamped with an Epoch value
        """
        currentmode = 0
        status_offset = 0

        currentmode = self.mode
        #self.test_data()
        self.screen_clear()
        #self.move_data()
        #self.screen_display()
        self.notrack_status_check()

        self.set_mode(currentmode, True)

        while not self.__endloop:
            if time.time() >= self.events[0]['time']:
                #Remove item zero from events and send it to action_event
                self.action_event(self.events.pop(0), currentmode)

            if status_offset > 250:
                status_offset = 0
                self.notrack_status_check()

            status_offset += 1
            time.sleep(0.2)


    def notrack_status_updated(self, newstatus: int) -> None:
        """
        Templace function which is called when NoTrack status changes
        Status is made up of the following bitwise values, which are declared on NoTrackAPI class
        STATUS_ENABLED = 1
        STATUS_DISABLED = 2
        STATUS_PAUSED = 4
        STATUS_INCOGNITO = 8
        STATUS_ERROR = 128

        Parameters:
            newstatus: New status for NoTrack containing bitwise values
        """
        #print(f'NoTrack Status {newstatus}')
        pass

    def screen_clear(self) -> None:
        pass

    def screen_display(self) -> None:
        pass
