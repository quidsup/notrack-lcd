#!/usr/bin/env python3
#Title       : Error Logging
#Description : Sets up logging level and is loaded as part of other modules
#Author      : QuidsUp
#Date        : 2021-12-27
#Version     : 1.0
#Usage       : N/A this module is loaded as part of other modules

#Standard imports
import logging
import sys

#Setup default logging config
logging.basicConfig(
    #level=logging.WARNING,
    level=logging.INFO,
    #level=logging.DEBUG,
    format='%(asctime)s %(levelname)-6s %(name)-18s %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S',
    handlers=[
        logging.StreamHandler(sys.stdout)
    ]
)
