#!/usr/bin/env python3
#Title       : NoTrack API Class
#Description :
#Author      : QuidsUp
#Date        : 2021-27-12
#Version     : 0.1
#Usage       : N/A this module is loaded elsewhere

"""
Carries out the following requests against NoTrack API
count_blocklisted_domains: Get the number of domains in the block list
count_total_queries      : Get the total number of queries
get_status               : Bitwise status of NoTrack (see status codes below)
queries_time             : List of the recent DNS queries sorted by Time
"""

#Global Imports
import configparser
import requests
import sys

#Local Imports
from lib.configloader import NoTrackConfig
import lib.errorlogger

#Create logger
logger = lib.errorlogger.logging.getLogger(__name__)


class NoTrackAPI:
    __api_url = ''
    __api_key = ''
    STATUS_ENABLED = 1
    STATUS_DISABLED = 2
    STATUS_PAUSED = 4
    STATUS_INCOGNITO = 8
    STATUS_ERROR = 128

    def __init__(self) -> None:
        #Load config file which contains API key
        self.__load_config()

        # Instantiate requestions session object
        self.__session = requests.session()
        #self.__session.auth = (self.__api_key)


    def __load_config(self) -> None:
        """
        Read variables from notracklcd.cfg
        """
        config = NoTrackConfig('notracklcd.cfg')

        self.__api_key = config.get('api', 'api_key')
        self.__api_url = config.get('api', 'url', fallback='http://192.168.1.2')

        if self.__api_url.endswith('/'):
            self.__api_url = f'{self.__api_url}admin/include/api.php'
        else:
            self.__api_url = f'{self.__api_url}/admin/include/api.php'


    def __download_data(self, action: str) -> list:
        """
        Download Data from NoTrack API

        Parameters:
            uri(str): API Query
        Returns:
            JSON formatted list of data
        """
        uri = ''
        uri = f'{self.__api_url}?api_key={self.__api_key}&action={action}'

        response = self.__session.get(uri)

        if response.status_code == 200:
            return response.json()
        else:
            self.__html_error(response.status_code)
            sys.exit(2)


    def __html_error(self, error_code: int) -> None:
        """
        Text interpretation for HTML error
        """
        if error_code == 500 or error_code == 502 or error_code == 503 or error_code == 504:
            logger.error(f'{error_code}: Server side error')
        elif error_code == 400:
            logger.error('400: Bad request')
        elif error_code == 401:
            logger.error('401: Unauthorised, invalid access')
        elif error_code == 403:
            logger.error('403: Unauthorised, invalid API token')
        elif logger.error == 404:
            logger.error('404: Page not found')
        elif error_code == 410:
            logger.error('410: File missing on server')
        elif error_code == 429:
            logger.error('429: Too many requests')
        else:
            logger.error(f'NoTrack API returned code {error_code}')


    def count_blocklisted_domains(self) -> dict:
        """
        Counts Number of Domains in Blocklist

        Returns:
            total number of domains in blocklist
        """
        data = self.__download_data('count_blocklisted_domains')

        if 'success' in data:
            return data

        return {'total': 0, 'success': False}


    def count_queries_today(self) -> dict:
        """
        Counts Queries Today

        Returns:
            count of allowed, blocked, total DNS queries for today
        """
        data = self.__download_data('count_queries_today')

        if 'success' in data:
            return data

        return {'allowed': 0, 'blocked': 0, 'total': 0, 'success': False}


    def count_total_queries(self) -> int:
        """
        Counts total number of DNS Queries in dnslog

        Returns:
            total DNS queries
        """
        data = self.__download_data('count_total_queries')

        if 'queries' in data:
            return data['queries']

        return 0


    def get_status(self) -> int:
        """
        Get NoTrack Status
        Returns:
            integer value of NoTrack status
        """
        data = self.__download_data('get_status')

        if 'status' in data.keys():
            return int(data['status'])

        return 0


    def queries_time(self, interval: int) -> dict:
        """
        Get recent indexed queries from dnslog
        NOTE: queries is a list of: Row Number, Date Time, Sys IP, DNS Query, Result, Allowed / Blocked

        Params:
            interval - time in minutes
        Return:
            dict of data containing queries, total, success
        """
        data = self.__download_data(f'recent_queries&interval={interval}')

        if 'success' in data:
            return data

        return {'queries': [], 'total': 0, 'success': False}
