#!/usr/bin/env python3

#Title: NoTrack LCD
#Description: NoTrack LCD uses the NoTrack API to output values on to a small 2, 3, or 4 line LCD Display
#Date : 2019-08-11


#The following values must be specified in notracklcd.conf:
#  api_key = 123456789abcdef     #Generate a key with NoTrack API setup
#  address = http://192.168.1.2  #NoTrack server address
#  lines = 2                     #adjust for your lcd display
#  rgb = 1 or 0                  #True or False if you have an RGB compatible display

from time import time, sleep
import subprocess
import os
import sys
import re
import math
from urllib.request import Request, urlopen
from urllib.error import HTTPError, URLError
import json
import RPi.GPIO as GPIO
import dot3k.lcd as lcd
import segments

#######################################
# Constants
#######################################
MAX_VIEW = 5
REGEX_RGB = match = "^(1?\d{1,2})%?,(1?\d{1,2})%?,(1?\d{1,2})%?$"
"""View Modes
    1: All
    2: Queries Today
    3: Queries Today as Percentage
    4: Blocklist count
"""

#######################################
# Settings Class
#######################################
class Settings:
    ETC_FOLDER = "/etc/notracklcd"
    HOME_FOLDER = ""
    MAIN_CONF = os.sep + "notracklcd.conf"
    VIEW_CONF = os.sep + "view.conf"

    api_key = ""
    lines = 2                                              #Number of lines on the LCD Display
    width = 16                                             #Number of characters wide
    address = ""                                           #NoTrack Server Address
    rgb = False
    mode = 1
    button_down = 17
    button_up = 4

    colour_blue = "12,38,90"
    colour_purple = "90,0,65"
    colour_green = '0,95,0'
    colour_salmon = "90,5,5"
    colour_red = "95,0,0"
    colour_yellow = "100,21,0"

    def __create_file(self, filename):
        """ Create Folder
        Create VIEW_CONF if it does not exist

        Args:
            filename
        Returns:
            None
        """
        #
        f = open(filename, "w")
        f.close();


    def __create_folder(self, foldername):
        """ Create Folder
        Create specified folder. Throw an error on failure

        Args:
            foldername
        Returns:
            None
        """
        try:
            os.mkdir(foldername)
        except OSError:
            print ("Creation of %s folder failed" % foldername)
        else:
            print ("Successfully created folder %s" % foldername)

    def __help_config(self):
        """ Help Config
        Print help message about how to setup config files for notracklcd

        Args:
            None
        Returns:
            None
        """
        print("Create file %s" % self.HOME_FOLDER + self.MAIN_CONF)
        print
        print("Provide the following settings:")
        print("api_key = 123456789abcdef     #Generate a key with NoTrack API setup")
        print("address = http://192.168.1.2  #NoTrack server address")
        print("lines = 2                     #adjust for your lcd display")
        print("rgb = 1 or 0                  #True or False if you have an RGB compatible display")
    
    
    def __filter_int(self, value, minimum, maximum, default):
        if value.isdigit():
            if (int(value) >= minimum) and (int(value) <= maximum):
                return int(value)
        
        return default
    
    def __filter_bool(self, value, default):
        if value == "0":
            return False
        elif value == "1":
            return True

        return default

    def load_config(self, filename):
        """ Load Config File
        Read variables from filename into this class

        Args:
            filename
        Returns:
            None
        """
        global MAX_VIEW
        f = open(filename, "r")
        
        #TODO Correctly validate that a line has been read print(len(match))
        for line in f:
            match = re.findall("^\s*([\w_]+)\s*=\s*([\w\.\/:]+)", line)

            if match[0][0] == "api_key":
                self.api_key = match[0][1]
            elif match[0][0] == "lines":
                self.lines = self.__filter_int(match[0][1].rstrip(), 1, MAX_VIEW, 2)
            elif match[0][0] == "address":
                self.address = match[0][1] + "/admin/include/api.php"
            elif match[0][0] == "mode":
                self.mode = self.__filter_int(match[0][1].rstrip(), 1, MAX_VIEW, 2)
            elif match[0][0] == "rgb":
                self.rgb = self.__filter_bool(match[0][1].rstrip(), False)

    def save_config(self):
        """ Save Config File
        save viewmode variables from this class into VIEW_CONF

        Args:
            None
        Returns:
            None
        """
        f = open(self.HOME_FOLDER + self.VIEW_CONF, "w")
        f.write("mode = " + str(self.mode))
        f.close()
    
    def test(self):
        print("api_key", self.api_key)
        print("lines", self.lines)
        print("address", self.address)
        print("rgb", self.rgb)
        print("mode", self.mode)

    def __init__(self):
        """ Initialize Settings
        load settings file

        Args:
            None
        Returns:
            None
        """

        #Setup home folder name depending on the OS
        if os.name == "posix":
            self.HOME_FOLDER = os.path.expanduser('~') + os.sep + '.notracklcd'

        elif os.name == "nt":
            self.HOME_FOLDER = os.path.join(os.getenv('APPDATA'), 'notracklcd')

        else:
            print('NoTrack-LCD has not been written for %s' % os.name)
            sys.exit(1)

        #Create config folder in users home folder if it does not exist
        if not os.path.isdir(self.HOME_FOLDER):
            self.__create_folder(self.HOME_FOLDER)

        #Try the /etc/notracklcd config file first
        if os.path.isfile(self.ETC_FOLDER + self.MAIN_CONF):
            self.load_config(self.ETC_FOLDER + self.MAIN_CONF)

        #Next try home folder for MAIN_CONF file
        if os.path.isfile(self.HOME_FOLDER + self.MAIN_CONF):
            self.load_config(self.HOME_FOLDER + self.MAIN_CONF)

        else:
            print('Error: no config available')
            self.__help_config()
            sys.exit(1)

        #Try and load the VIEW_CONF
        if os.path.isfile(self.HOME_FOLDER + self.VIEW_CONF):
            self.load_config(self.HOME_FOLDER + self.VIEW_CONF)

        else:
            self.__create_file(self.HOME_FOLDER + self.VIEW_CONF)

        #Make sure a server address has been loaded from a config file
        if not self.address:
            print('Error: no address specified for NoTrack server in %s' % self.HOME_FOLDER + self.MAIN_CONF)
            sys.exit(1)


#######################################
# NoTrack API Class
#######################################
class NoTrackAPI:
    data = {}                                              #JSON Data from API
    queries = {}                                           #Recent DNS Queries
    timedata = {}                                          #UTC Time each query took place
    current_count = 0
    status = 0
    
    STATUS_ENABLED = 1;
    STATUS_DISABLED = 2;
    STATUS_PAUSED = 4;
    STATUS_INCOGNITO = 8;
    STATUS_NOTRACKRUNNING = 64;
    STATUS_ERROR = 128;

    def __download_data(self, action):
        """Download Data from API
        Build Request from URL, api_key, action
        Check Return code from webserver
        Encode page returned as JSON Dictionary
        Add UTC time of query to timedata

        Args:
            API Query
        Return:
            JSON Encoded Dictionary
        """

        req = Request(settings.address + "?api_key=" + settings.api_key + "&action=" + action)
        try:
            response = urlopen(req, timeout=5)
        except HTTPError as e:
            print(e.code)
            if e.code == 400:
                print('Invalid Request')
                return {'message': 'Invalid Request'}
            elif e.code == 401:
                print('Invalid API Key')
                sys.exit(1)
            elif e.code == 410:
                print('File missing on server')
                return {'message': 'File missing on server'}

        except URLError as e:
            if hasattr(e, 'reason'):
                print('Failed to find NoTrack server')
                print('Reason: ', e.reason)
                sys.exit(113)
            elif hasattr(e, 'code'):
                print('NoTrack server was unable to fulfill the request')
                print('Error code: ', e.code)
                sys.exit(113)
        else:
            res_body = response.read()
            self.timedata[action] = time()
            return json.loads(res_body.decode("utf-8"))


    def get_data(self, api_query):
        """Get JSON Data from and add it to data Dictionary
        Args:
            API Query
        Return:
            None
        """

        #print('Contacting API')                           #Uncomment for debugging
        self.data = {**self.__download_data(api_query), **self.data}


    def get_recent_queries(self):
        """Get Recent Queries
        Set queries dictionary with the result from API call 
        Args:
            None
        Return:
            None
        """
        self.queries = self.__download_data('recent_queries')


    def get_status(self):
        """Get NoTrack Status
        Set integer value of status with the result from API call
        Args:
            API Query
        Return:
            True on Success
            False on Failure
        """
        statusdata = self.__download_data('get_status')
        
        if (not 'status' in statusdata):
            return False
      
        self.status = int(statusdata['status'])
        
        return True
      

    def is_update_required(self, api_query, interval):
        """Check API Update is Required
        Check time recorded in timedata and see if its still less than interval
        Args:
            API Query, update interval in seconds
        Return:
            True if update needed or no record found
            False if still under update interval
        """

        if (api_query in self.timedata):
            if (self.timedata[api_query] + interval < time()):
                return True
        else:
            return True

        return False

    def __init__(self):
        self.current_count = 0


def button_press(channel):
    """Action a button press
    Args:
        GPIO Channel pressed
    Return:
        None
    """

    if (channel == settings.button_up):
        settings.mode += 1
        change_view_mode()
    elif (channel == settings.button_down):
        settings.mode -= 1
        change_view_mode()


def set_display_colour(colour):
    """Set LCD Background Colour
    Args:
        Colour in the form of "R%,G%,B%"
    Return:
        None
    """
    global REGEX_RGB
    global r, g, b
    
    matches = re.findall(REGEX_RGB, colour)
    
    if (len(matches) == 0):
        print("Error: Invalid RGB Colours")
        return

    r.ChangeDutyCycle(int(matches[0][0]))
    g.ChangeDutyCycle(int(matches[0][1]))
    b.ChangeDutyCycle(int(matches[0][2]))
    
    
def check_status():
    """Check Status
    Get Config.status from NoTrack API
    Choose a Display colour based on the bitwise value
    """
    
    #Get the latest status from config.status
    notrack.get_status()

    #Choose a single colour to use, bearing in mind that multiple statuses happen
    if (notrack.status & notrack.STATUS_ERROR):
        set_display_colour(settings.colour_red)
    elif (notrack.status & notrack.STATUS_NOTRACKRUNNING):
        set_display_colour(settings.colour_green)
    elif (notrack.status & notrack.STATUS_DISABLED):
        set_display_colour(settings.colour_salmon)
    elif (notrack.status & notrack.STATUS_PAUSED):
        set_display_colour(settings.colour_yellow)
    elif (notrack.status & notrack.STATUS_INCOGNITO):
        set_display_colour(settings.colour_purple)
    elif (notrack.status & notrack.STATUS_ENABLED):
        set_display_colour(settings.colour_blue)
    
    
def clear():
    """Clear Screen
    Check the OS type and issue appropriate command:
    cls for Windows
    clear for Linux / Mac
    """

    if os.name == 'nt': 
        os.system('cls') 

    else:
        os.system('clear') 


def change_view_mode():
    """Prepare to change view mode
    Check settings.mode is within valid confines
    Save config
    Show the name of the new view
    """

    global MAX_VIEW
    ypos = 1

    #Check settings.mode is within valid confines
    if (settings.mode < 1):
        settings.mode = MAX_VIEW
    elif (settings.mode > MAX_VIEW):
        settings.mode = 1

    settings.save_config()
    clear()
    lcd.clear()

    print('Mode', settings.mode)

    if (settings.lines == 1):
        ypos = 0
    else:
        write_center('Mode: ' + str(settings.mode), 0)

    #Show the name of the new view mode
    if (settings.mode == 1):
        print('Cycle Through')
        write_center('Cycle Through', ypos)
    elif (settings.mode == 2):
        print('DNS Queries Today')
        write_center('Queries Today', ypos)
    elif (settings.mode == 3):
        print('DNS Queries Today %')
        write_center('Queries Today %', ypos)
    elif (settings.mode == 4):
        print('Blocklist Count')
        write_center('Blocklist Count', ypos)
    elif (settings.mode == 5):
        print('Recent Queries')
        write_center('Recent Queries', ypos)

    sleep(1)


def show_intro():
    #TODO different display widths
    for charpos in range(1,9):
        for xpos in range(charpos,charpos+4):
            for index, char in enumerate(segments.notrackmsg[xpos]):
                lcd.set_cursor_position(index % 3 + (xpos -charpos) * 4, math.floor(index / 3))
                lcd.write(chr(char))
            if charpos == 1:
                sleep(0.3)
        sleep(0.4)

    sleep(1)


def show_queries_today():
    """Show Queries Today
    Check valid data has been returned from API
    Print output in appropriate format for number of lines
    """

    #Check we have the necessary data and that its new enough
    if (not 'queries' in notrack.data) or (notrack.is_update_required('count_dnsqueries_today', 1200)):
        notrack.get_data('count_dnsqueries_today')
        #Warning: Missing data, leave function
        if (not 'queries' in notrack.data):
            return

    print('Total Queries:' ,notrack.data['queries'])
    print('Allowed:', notrack.data['allowed'])
    print('Blocked:', notrack.data['blocked'])
    #print('  Local:', notrack.data['local'])

    lcd.clear()
    
    if settings.lines == 1:
        write_left('Total Queries:' + notrack.data['queries'], 0)
    elif settings.lines == 2:
        write_left('Allowed:' + notrack.data['allowed'], 0)
        write_left('Blocked:' + notrack.data['blocked'], 1)
    elif settings.lines == 3:
        write_left('Allowed: ' + notrack.data['allowed'], 0)
        write_left('Blocked: ' + notrack.data['blocked'], 1)
        #write_left('  Local: ' + notrack.data['local'], 2)
    else:
        write_left('Total Queries:' + notrack.data['queries'], 0)
        write_left('Allowed:' + notrack.data['allowed'], 1)
        write_left('Blocked:' + notrack.data['blocked'], 2)
        #write_left('  Local:' + notrack.data['local'], 3)


def show_queries_today_percentage():
    """Show Queries Today as Percentage
    Check valid data has been returned from API
    Print output in appropriate format for number of lines
    """

    #Check we have the necessary data and that its new enough
    if (not 'queries' in notrack.data) or (notrack.is_update_required('count_dnsqueries_today', 1200)):
        notrack.get_data('count_dnsqueries_today')
        #Warning: Missing data, leave function
        if (not 'queries' in notrack.data):
            return

    allowed = float(0)
    blocked = float(0)
    local = float(0)
    
    allowed = round((int(notrack.data['allowed']) / int(notrack.data['queries'])) * 100, 1)
    blocked = round((int(notrack.data['blocked']) / int(notrack.data['queries'])) * 100, 1)
    #local = round((int(notrack.data['local']) / int(notrack.data['queries'])) * 100, 1)

    print('Allowed: %3.2f%%' % allowed)
    print('Blocked: %3.2f%%' % blocked)
    print('  Local: %3.2f%%' % local)

    lcd.clear()

    if settings.lines == 1:
        write_left('Allowed: ' + str(allowed) + '%', 0)
    elif settings.lines == 2:
        write_left('Allowed: ' + str(allowed) + '%', 0)
        write_left('Blocked: ' + str(blocked) + '%', 1)
    else:
        write_left('Allowed: ' + str(allowed) + '%', 0)
        write_left('Blocked: ' + str(blocked) + '%', 1)
        write_left('  Local: ' + str(local) + '%', 2)


def show_blocklist_count():
    """Show Blocklist Count
    Check valid data has been returned from API
    Print output
    """

    #Check we have the necessary data and that its new enough
    if (not 'blocklists' in notrack.data) or (notrack.is_update_required('count_blocklists', 2400)):
        notrack.get_data('count_blocklists')
        #Warning: Missing data, leave function
        if (not 'blocklists' in notrack.data):
            return

    print('Blocklist count:', notrack.data['blocklists'])

    lcd.clear()

    if (settings.lines == 1):
        write_center(notrack.data['blocklists'], 1)
    elif (settings.lines == 2) or (settings.lines == 3):
        write_center('Blocklist Count:', 0)
        write_center(notrack.data['blocklists'], 1)
    else:
        #Base y position on half of device height
        write_center('Blocklist Count:', math.ceil(settings.lines / 2) - 1)
        write_center(notrack.data['blocklists'], math.floor((settings.lines / 2) + 1))


def show_recent_queries():
    """Show Recent Queries
    Check valid data has been returned from API
    Vertical scrolling is done using notrack.current_count and a for loop set to number of lines
    Shorten the allow / block / local to "+/-/l"
    TODO horizontal scroll
    """
    dns_result = ""
    dns_request = ""
    querypos = 0                                           #Position in Dictionary
    querylen = 0                                           #Length of Dictionary
    
    if (notrack.is_update_required('recent_queries', 240)):
        print('update needed')
        notrack.current_count = 0
        notrack.get_recent_queries()

    lcd.clear()

    #An error code could mean no data or SQL error
    if ('error_code' in notrack.queries):
        if (notrack.queries['error_code'] == 'no_data_found'):
            write_left('No recent data', 0)
        else:
            write_left('Error')
            print(notrack.queries['error_message'])
        return

    querylen = len(notrack.queries)

    print("Length: %d" % querylen)

    #Reset vertical scrolling when we have reached the end
    if (notrack.current_count >= querylen):
        notrack.current_count = 0

    #Loop through number of lines in the LCD display
    for i in range(settings.lines):

        #Set the actual position in the dictionary
        querypos = i + notrack.current_count
        if (querypos >= querylen):
            break

        #Show the data returned from API
        dns_request = notrack.queries[querypos][3]
        if (notrack.queries[querypos][4] == 'A'):
            dns_result = '+'
        elif(notrack.queries[querypos][4] == 'B'):
            dns_result = '-'
        elif(notrack.queries[querypos][4] == 'L'):
            dns_result = 'l'

        #Output the result
        #TODO horizontal scrolling
        print(querypos, dns_result, dns_request)
        write_left(dns_result + dns_request, i)


def write_center(msg, y):
    """Write Line to center of LCD Device
    Use string.center to pad string to device width
    """
    lcd.set_cursor_position(0, y)
    lcd.write(msg.center(settings.width))


def write_left(msg, y):
    """Write Line to Left of LCD Device
    Trim any characters longer than device width
    """
    lcd.set_cursor_position(0, y)
    lcd.write(msg[:settings.width])


#######################################
# Global Variables
#######################################
settings = Settings()
notrack = NoTrackAPI()


GPIO.setmode(GPIO.BCM)
GPIO.setup(settings.button_up, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(5, GPIO.OUT)
GPIO.setup(6, GPIO.OUT)
GPIO.setup(13, GPIO.OUT)
r = GPIO.PWM(5, 100)
g = GPIO.PWM(6, 100)
b = GPIO.PWM(13, 100)

r.start(0)
g.start(0)
b.start(0)

lcd.create_char(0, segments.segleft)
lcd.create_char(1, segments.segright)
lcd.create_char(2, segments.segtop)
lcd.create_char(3, segments.segbottom)
lcd.create_char(4, segments.segtopleft)
lcd.create_char(5, segments.segtopright)
lcd.create_char(6, segments.segbottomleft)
lcd.create_char(7, segments.segbottomright)
lcd.set_contrast(15)
lcd.clear()

GPIO.add_event_detect(settings.button_up, GPIO.RISING, callback=button_press, bouncetime=1500)


change_view_mode()
set_display_colour(settings.colour_blue)
#show_intro()

try:
    while True:
        clear()
        check_status()

        if settings.mode == 1:
            if (notrack.current_count >= 0) and (notrack.current_count < 10):
                show_queries_today()
            elif (notrack.current_count >= 10) and (notrack.current_count < 20):
                show_queries_today_percentage()
            elif (notrack.current_count >= 20) and (notrack.current_count < 30):
                show_blocklist_count()
            else:
                notrack.current_count = 0
        elif settings.mode == 2:
            show_queries_today()
        elif settings.mode == 3:
            show_queries_today_percentage()
        elif settings.mode == 4:
            show_blocklist_count()
        elif settings.mode == 5:
            show_recent_queries()
    
        notrack.current_count += 1
        sleep(4)

except KeyboardInterrupt:
    pass

r.stop()
g.stop()
b.stop()
lcd.clear()
GPIO.cleanup()
