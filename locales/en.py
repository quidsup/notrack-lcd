#!/usr/bin/env python3
#Title       : NoTrack Display - Locals en
#Description : English Strings used in NoTrack Display
#Author      : QuidsUp
#Date        : 2021-31-12
#Version     : 0.1
#Usage       : N/A module is loaded elsewhere

strings = {
    'short' : {
        'mode' : 'Mode',
        #Mode 1
        'queries allowed' : 'Allowed',
        'queries blocked' : 'Blocked',
        'total queries' : 'Total Queries',
        #Mode 2
        'domains in blocklist' : 'Blocklist',
        'domains' : 'Domains'
    },
    'long' : {
        'mode' : 'Mode',
        #Mode 1
        'queries allowed' : 'Queries Allowed',
        'queries blocked' : 'Queries Blocked',
        'total queries' : 'Total DNS Queries',
        #Mode 2
        'domains in blocklist' : 'Domains In Blocklist',
        'domains' : '',
    },
}
