#!/usr/bin/env python3
#Title       : NoTrack Display - Locals
#Description :
#Author      : QuidsUp
#Date        : 2021-31-12
#Version     : 0.1
#Usage       : N/A module is loaded elsewhere

import locales.en
strings = dict()

def setuplocale(lang, size):
    global strings

    if lang == 'en':
        strings = locales.en.strings[size]


def translate(s):
    global strings
    return strings[s]
