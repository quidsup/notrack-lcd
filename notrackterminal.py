#!/usr/bin/env python3
#Title       : NoTrack DOT3K
#Description :
#Author      : QuidsUp
#Date        : 2021-27-12
#Version     : 0.1
#Usage       :

#Global Imports
import configparser
import os
import requests
import sys

#Local Imports
import lib.errorlogger
from lib.displayhandler import NoTrackDisplay

#Create logger
logger = lib.errorlogger.logging.getLogger(__name__)

class NoTrackTerminal(NoTrackDisplay):
    def screen_clear(self) -> None:
        return
        if os.name == 'posix':
            os.system('clear')
        else:
            os.system('cls')

    def screen_display(self) -> None:
        for line in self.displaydata:
            print(line)

def main():
    disp = NoTrackTerminal()
    disp.load_config()
    disp.start_loop()

    #print(api.get_status())
    #print(api.count_queries_today())
    #print(api.count_total_queries())
    #print(api.recent_queries(1))
    #print(api.count_blocklisted_domains())


if __name__ == "__main__":
    main()
