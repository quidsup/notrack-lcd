#!/usr/bin/env python3
#Title       : NoTrack DOT3K
#Description :
#Author      : QuidsUp
#Date        : 2021-27-12
#Version     : 0.1
#Usage       :

#Global Imports
import configparser
import requests
import sys

#3rd Party Imports
import RPi.GPIO as GPIO
import dot3k.lcd as lcd

#Local Imports
import lib.errorlogger
from lib.displayhandler import NoTrackDisplay

#Create logger
logger = lib.errorlogger.logging.getLogger(__name__)

class NoTrackDot3K(NoTrackDisplay):
    colour_blue = [12,38,90]
    colour_purple = [90,0,65]
    colour_green = [0,95,0]
    colour_salmon = [90,5,5]
    colour_red = [95,0,0]
    colour_yellow = [100,21,0]

    def __init__(self) -> None:
        """
        """
        super().__init__()

        GPIO.setmode(GPIO.BCM)
        #GPIO.setup(settings.button_up, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(5, GPIO.OUT)
        GPIO.setup(6, GPIO.OUT)
        GPIO.setup(13, GPIO.OUT)
        self.r = GPIO.PWM(5, 100)
        self.g = GPIO.PWM(6, 100)
        self.b = GPIO.PWM(13, 100)

        self.r.start(0)
        self.g.start(0)
        self.b.start(0)
        lcd.set_contrast(15)
        #self.screen_colour(self.colour_blue)

    def __del__(self) -> None:
        self.r.stop()
        self.g.stop()
        self.b.stop()
        lcd.clear()
        GPIO.cleanup()


    def notrack_status_updated(self, newstatus: int) -> None:
        """
        Templace function which is called when NoTrack status changes
        Status is made up of the following bitwise values, which are declared on NoTrackAPI class
        STATUS_ENABLED = 1
        STATUS_DISABLED = 2
        STATUS_PAUSED = 4
        STATUS_INCOGNITO = 8
        STATUS_ERROR = 128

        Parameters:
            newstatus: New status for NoTrack containing bitwise values
        """
        #print(f'NoTrack Status {newstatus}')
        if newstatus & self.api.STATUS_DISABLED:
            self.screen_colour(self.colour_salmon)
        elif newstatus & self.api.STATUS_PAUSED:
            self.screen_colour(self.colour_yellow)
        elif newstatus & self.api.STATUS_INCOGNITO:
            self.screen_colour(self.colour_purple)
        elif newstatus & self.api.STATUS_ENABLED:
            self.screen_colour(self.colour_blue)
        else:
            self.screen_colour(self.colour_red)

    def screen_colour(self, colour):
        """Set LCD Background Colour
        Args:
            Colour in the form of "R%,G%,B%"
        Return:
            None
        """
        self.r.ChangeDutyCycle(colour[0])
        self.g.ChangeDutyCycle(colour[1])
        self.b.ChangeDutyCycle(colour[2])

    def screen_clear(self) -> None:
        lcd.clear()

    def screen_display(self) -> None:
        y = 0
        for line in self.displaydata:
            lcd.set_cursor_position(0, y)
            lcd.write(line[:self.cols])
            y += 1


def main():
    disp = NoTrackDot3K()
    disp.load_config()
    disp.start_loop()

    #print(api.get_status())
    #print(api.count_queries_today())
    #print(api.count_total_queries())
    #print(api.recent_queries(1))
    #print(api.count_blocklisted_domains())


if __name__ == "__main__":
    main()
